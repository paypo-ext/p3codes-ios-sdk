//
//  MainViewController.swift
//  PayPoLibExaples
//
//  Created by Paweł Łukasiak on 05/07/2021.
//

import UIKit

class MainViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet private weak var urlTextField: UITextField!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }
        
    // MARK: - Actions Methods

    @IBAction func nextButtonAction(_ sender: Any) {
        showPayPoWebView()
    }
    
    // MARK: - Private Methods

    private func setupSubviews() {
        urlTextField.text = "https://panel.sandbox.paypo.pl/registration"
        urlTextField.delegate = self
    }
    
    private func showPayPoWebView() {
        guard let url = urlTextField.text else { return }
        let payPoWebViewController = PayPoWebViewController(transactionUrl: url)
        payPoWebViewController.modalPresentationStyle = .popover
        present(payPoWebViewController, animated: true, completion: nil)
//        navigationController?.pushViewController(payPoWebViewController, animated: true)
        payPoWebViewController.didFinishWithSuccess = {
            payPoWebViewController.dismiss(animated: true) {
                // do some staff
            }
        }
        
        payPoWebViewController.didFinishWithFailure = {
            payPoWebViewController.dismiss(animated: true) {
                // do some staff
            }
        }
        
        payPoWebViewController.didClickCloseButton = {
            payPoWebViewController.dismiss(animated: true) {
                // do some staff
            }
        }
    }
}

extension MainViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
