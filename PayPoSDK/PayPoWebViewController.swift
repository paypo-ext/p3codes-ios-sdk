//
//  PayPoWebViewController.swift
//  PayPoLib
//
//  Created by Paweł Łukasiak on 06/07/2021.
//

import UIKit
import WebKit

// MARK: - PayPoWeb View Controller Delegate

public protocol PayPoWebViewControllerDelegate: NSObject {
    func didFinishTransaction()
    func didFinishTransactionWithFailure()
    func didClickCloseButton()
}

// MARK: - PayPoWeb View Controller

public final class PayPoWebViewController: UIViewController {
    
    // MARK: - Variables
    
    public var didFinishWithSuccess: (() -> ())?
    public var didFinishWithFailure: (() -> ())?
    public var didClickCloseButton: (() -> ())?
    public weak var delegate: PayPoWebViewControllerDelegate?
    
    // MARK: - Private Variables
    
    private var webView: WKWebView!
    private var closeButton: UIButton!
    private var backButton: UIButton!
    private var buttonsStackView: UIStackView!
    private let transactionUrl: String?
    
    private let successParameterValueString = "success"
    private let failureParameterValueString = "failure"
    private let parameterString = "transaction"
    
    // MARK: - Inits
    
    public init(transactionUrl: String) {
        self.transactionUrl = transactionUrl
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubViews()
    }
    
    // MARK: - Private Methods
    
    private func setupSubViews() {
        view.backgroundColor = .white
        setupWebView()
        setupButtonsStackView()
    }
    
    private func setupWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        
        NSLayoutConstraint.activate([
            self.webView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.webView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.webView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50),
            self.webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
        guard let urlString = transactionUrl,
              let myURL = URL(string: urlString) else { return }
        
        let myRequest = URLRequest(url: myURL)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.load(myRequest)
    }
    
    private func setupButtonsStackView() {
        closeButton = UIButton()
        closeButton.setImage(UIImage(named: "cross.png") , for: .normal)
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
        
        backButton = UIButton()
        backButton.setImage(UIImage(named: "left_black_arrow.png") , for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        backButton.isEnabled = webView.canGoBack

        //Stack View
        buttonsStackView = UIStackView()
        buttonsStackView.axis = NSLayoutConstraint.Axis.horizontal
        buttonsStackView.distribution = UIStackView.Distribution.equalSpacing
        buttonsStackView.alignment = UIStackView.Alignment.fill

        buttonsStackView.addArrangedSubview(backButton)
        buttonsStackView.addArrangedSubview(UIView())
        buttonsStackView.addArrangedSubview(closeButton)

        buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(buttonsStackView)

        NSLayoutConstraint.activate([
            buttonsStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10.0),
            buttonsStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20.0),
            buttonsStackView.topAnchor.constraint(equalTo: self.view.topAnchor),
            buttonsStackView.bottomAnchor.constraint(equalTo: self.webView.topAnchor)
        ])
    }
    
    private func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    // MARK: - Actions Methods
    
    @objc private func backButtonAction() {
        guard webView.canGoBack else { return }
        backButton.isEnabled = webView.backForwardList.backList.count > 1
        
        webView.goBack()
    }
    
    @objc func closeButtonAction() {
        didClickCloseButton?()
        delegate?.didClickCloseButton()
    }
}

// MARK: - WKUIDelegate

extension PayPoWebViewController: WKUIDelegate {
    public func webViewDidClose(_ webView: WKWebView) {
        didFinishWithSuccess?()
        delegate?.didFinishTransaction()
    }
}

// MARK: - WKUIDelegate

extension PayPoWebViewController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        guard let urlString = navigationAction.request.url?.absoluteString else {
            decisionHandler(.allow)
            return
        }
        if successParameterValueString == getQueryStringParameter(url: urlString, param: parameterString) {
            didFinishWithSuccess?()
            delegate?.didFinishTransaction()
        } else if failureParameterValueString == getQueryStringParameter(url: urlString, param: parameterString) {
            didFinishWithFailure?()
            delegate?.didFinishTransactionWithFailure()
        }
        decisionHandler(.allow)
        backButton.isEnabled = webView.canGoBack
    }
}
